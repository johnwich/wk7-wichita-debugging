# This is a header for the application
# You should read this header and insert your name and your date below as part of the peer review
# This is a typical part of any program
# Author: John Wichita
# Creation Date: 12-3-2021
# Below is a simple program with 10 issues (some are syntax errors and some are logic errors.  You need to identify the issues and correct them.

import random
import time

def displayIntro():
	print('''You are in a land full of dragons. In front of you,
	you see two caves. In one cave, the dragon is friendly
	and will share his treasure with you. The other dragon
	is greedy and hungry, and will eat you on sight.''')
	print()

def chooseCave():
    chosenCave = ''
    while chosenCave != '1' and chosenCave != '2':
        print('Which cave will you go into? (1 or 2)')
        chosenCave = input()
        print(chosenCave)
        
    return chosenCave

def checkCave(chosenCave):
	print('You approach the cave...')
	#sleep for 2 seconds
	time.sleep(.2)
	print('It is dark and spooky...')
	#sleep for 2 seconds
	time.sleep(.3)
	print('A large dragon jumps out in front of you! He opens his jaws and...')
	print()
	#sleep for 2 seconds
	time.sleep(.2)
	friendlyCave = random.randint(1, 2)

	if chosenCave == str(friendlyCave):
		print('Gives you his treasure!')
	else:
		print ('Gobbles you down in one bite!')

playAgain = 'yes'
while playAgain == 'yes' or playAgain == 'y':
	displayIntro()
	chooseCave()
	checkCave()
    
	print('Do you want to play again? (yes or no)')
	playAgain = input()
	if playAgain == "no":
		print("Thanks for playing")



# 1 issue tabs indentation - 2?(not issue) issue switch to input from print - 3 remove s from caves to make it cave - 4 update tab spacing on return cave - 5 paranthesis around gobbles statemnt - 6 typo at ent planing to playing - 7 double == for playagain since it is a string - 8 chooseCave() not defined, removed (); actually that needs to be a function, so changed again ln 48 - 9 
